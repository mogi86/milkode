# Milkode
Milkodeインストール済みのboxファイルを使用しての環境構築手順
  
##Vagrantでの環境構築  
※VirtualBoxとVagrantがインストールされていることが前提  

* 下記からboxをダウンロード  
  https://bitbucket.org/mogi86/milkode/downloads  

* ダウンロードしたboxをローカルの任意の場所に格納  

* box追加  
  vagrant box add box名 box格納パス/boxファイル名  
  
* Vagrantfileの作成  
  vagrant init box名  
  
* Vagrantfileの修正  
  下記コメントを外す  
  # config.vm.network "private_network", ip: "192.168.33.10"   
   
* 仮想マシン起動  
  vagrant up  
  
##仮想マシン上でのMilkode設定・起動  

* Milkodeで検索対象としたいプロジェクトを追加  
  milk add ソースファイルのあるパス  
  　例）milk add ~/source/repo_a    
 
* Milkode起動  
  milk web -o 0.0.0.0 -n  
  
* ブラウザで表示 
  「http://{仮想マシンのIPアドレス}:9292/home」にアクセス  
